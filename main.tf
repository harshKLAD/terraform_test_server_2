provider "aws" {
  region     = "${var.aws_region}"
}

# Creating VPC
resource "aws_vpc" "demovpc" {
  cidr_block       = "${var.vpc_cidr}"
  instance_tenancy = "default"

  tags = {
    Name = "Test VPC"
  }
}

# Creating Ubuntu-EC2 instance
resource "aws_instance" "UBUNTU_EC2" {
  ami = "ami-043a72cf696697251"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.deployer.key_name}"

  tags = {
    Name = "terraform-test-EC2"
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "${var.RSA_PUB_KEY}"
}

terraform {
  backend "s3" {
    bucket = "terraform-state-harsh"
    key    = "terraform_State_File"
    region = "ca-central-1"
  }
}

# Generate inventory file
resource "local_file" "inventory" {
 filename = "./inventory/hosts.ini"
 content = <<EOF
[webserver]
${aws_instance.UBUNTU_EC2.public_ip}
EOF
}
